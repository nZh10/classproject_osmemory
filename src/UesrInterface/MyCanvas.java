package UesrInterface;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyCanvas extends Canvas
{
    static private Random rand= new Random();
    static private int lastColorPos = 0;
    static private List<Color> colors = new ArrayList<>();

    private Color color;
    private int wide;
    private int height;
    private int tag;
    private int start;
    private int length;

    MyCanvas(int start, int wide, int height, int tag, boolean isUsed)
    {
        this.wide = (int) (wide * 1200 / 640.0); // 1200 / 640
        if(colors.size()==0) // rainbow colors initialize if empty list
        {
            for (int r=0; r<100; r++) colors.add(new Color(r*255/100,       255,         0));
            for (int g=100; g>0; g--) colors.add(new Color(      255, g*255/100,         0));
            for (int b=0; b<100; b++) colors.add(new Color(      255,         0, b*255/100));
            for (int r=100; r>0; r--) colors.add(new Color(r*255/100,         0,       255));
            for (int g=0; g<100; g++) colors.add(new Color(        0, g*255/100,       255));
            for (int b=100; b>0; b--) colors.add(new Color(        0,       255, b*255/100));
            colors.add(new Color(        0,       255,         0));
        }
        if(isUsed) this.color = generateRandomColor();
        else this.color = Color.WHITE;
        this.wide = (int) (wide * 1.875); // 1200 / 640
        this.height = height;
        this.tag = tag;
        length = wide;
        setSize(this.wide, this.height);
        this.start = start;
    }

    private Color generateRandomColor()
    {
        int now=(int)(lastColorPos+(Math.random()*60)+60)%colors.size();
        Color ret = colors.get(now);
        lastColorPos=now;
        return ret;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics pen = g;
        pen.setColor(color);
        pen.fillRect(0, 0, wide, height);

        pen.setColor(Color.BLACK);
        if(!color.equals(Color.WHITE) && (wide-50)/2>0)
        {
            pen.setFont(new Font("方正静蕾简体", Font.PLAIN, 100));
            pen.drawString(String.valueOf(tag), (wide - 50)/2, (int) (
                    400 * (1 - 0.618)));
        }
        pen.setFont(new Font("Noto Sans SC",Font.BOLD,12));
        int randHeight= rand.nextInt(290) + 30 ;
        pen.drawString(String.valueOf(start) + "K", 5, randHeight);
        pen.drawString(String.valueOf(start + length) + "K", wide - 40, 350-randHeight);
    }
}