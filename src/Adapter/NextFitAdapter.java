package Adapter;

import Core.MemoryBlock;
import UesrInterface.UI;

import java.util.ListIterator;


// TODO: Work of Task 4
public class NextFitAdapter extends Adapter
{
    public NextFitAdapter(UI ui, int maxMemorySize)
    {
        super(ui, maxMemorySize);
    }
    public NextFitAdapter(UI ui)
    {
        super(ui);
    }
    private static int pos=0;

    private int insertAndGetBlockId(int length, int tag, MemoryBlock mBlk)
    {
        MemoryBlock newMBlk = new MemoryBlock(mBlk.getStart(), mBlk.getStart()+length, tag);
        mBlk.setStart(mBlk.getStart()+length);
        int i=table.indexOf(mBlk);
        table.add(i, newMBlk);
        tags.add(i);
        ui.appendBoard("Insertion Accomplished.\n");
        return i;
    }

    @Override
    public void insert(int length, int tag)
    {
    	boolean done=false;
        System.out.println("======");
    	
    	ListIterator<MemoryBlock> tableIterator = table.listIterator();
    	while(pos-- != 0) tableIterator.next();    //the position last assigned
    	while(tableIterator.hasNext())
    	{
    		MemoryBlock mBlk = tableIterator.next();
    	   	if(!mBlk.getMemoryState() && mBlk.getLength() >= length)   //the partition is not allocated and the length is satisfactory
    	   	{                                                          //allocation process
    	       	 done = true;
    	       	 pos=insertAndGetBlockId(length, tag, mBlk);
    	       	 break;
    	   	}
       	}
    	if(!tableIterator.hasNext() && !done)  //the size of the last free partition is not sufficient
    	{
    		MemoryBlock mBlk = table.getFirst();  //return the first free partition
    		while(pos-- != 0)
    		{
    			int index = table.indexOf(mBlk);
    			if(!mBlk.getMemoryState() && mBlk.getLength() >= length)
        	   	{
                    done = true;
                    pos=insertAndGetBlockId(length, tag, mBlk);
                    break;
        	   	}
    			else mBlk = table.get(index+1);
    		}
    	}
        if(!done)   //allocation failure
        {
            ui.appendBoard("Insertion Failed. Short of memory.\n");
        }

    }
}
