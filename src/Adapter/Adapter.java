package Adapter;

import Core.MemoryBlock;
import UesrInterface.UI;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public abstract class Adapter
{
    public LinkedList<MemoryBlock> table;
    public ArrayList<Integer> tags;
    UI ui;

    Adapter(UI ui, int maxMemorySize)
    {
        this.ui=ui;
        table = new LinkedList<>();
        tags = new ArrayList<>();
        MemoryBlock blank = new MemoryBlock(0, maxMemorySize ,0);
        blank.setMemoryState(false);
        tags.add(0);
        table.add(blank);
    }
    Adapter(UI ui)
    {
        this(ui,640);
    }

    public abstract void insert(int length, int tag);

    public void release(int tag)
    {
        // release memory, and merge empty memory blocks from left to right.
        try
        {
            if(tags.get(tag) == -1)
            {
                ui.appendBoard("Job"+tag+" has been released.\n");
                return;
            }
            tags.set(tag, -1);
        }
        catch(RuntimeException err)
        {
            ui.appendBoard("No such memory block.\n");
            return;
        }

        ListIterator<MemoryBlock> tableIterator = table.listIterator();
        while(tableIterator.hasNext())
        {
            MemoryBlock nowBlk = tableIterator.next();
            if (nowBlk.getTag() == tag)
            {
                nowBlk.setMemoryState(false);
                ui.appendBoard("Job " + tag + " is released.\n");
                break;
            }
        }
        tableIterator = table.listIterator();
        while(tableIterator.hasNext())
        {
            MemoryBlock nowBlk = tableIterator.next();
            if(!nowBlk.getMemoryState() && tableIterator.hasNext())
            {
                MemoryBlock nxtBlk = tableIterator.next();
                if(!nxtBlk.getMemoryState())
                {
                    nowBlk.setEnd(nxtBlk.getEnd());
                    tableIterator.remove();
                }
                nowBlk=tableIterator.previous(); // fix the bug that some elements will skip the loop check.
            }
        }
    }
}
